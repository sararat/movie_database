# Movie database
a simple webapp for displaying details about movies

# Table of Contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Screenshots](#screenshots)
* [Features](#features)

# General info
My main purpose for making this project was practising developing an independent project from scratch. 
# Technologies
Spring Boot, MySQL, Angular JS, Bootstrap 4
# Setup
to run this project, use IDE or command line; use relational database software; 
# Screenshots
![](/images/movieDb.png)

# Features 
 1. Displaying details about movies paginated
 2. Comment a movie
 3. Search by title / director
 4. Edit or delete a movie from the database
 

